var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('projects', { title: 'Express' });
});

router.get('/homelab', function(req, res, next) {
  res.render('projects_homelab', { title: 'Express' });
});

router.get('/capstone', function(req, res, next) {
  res.render('projects_capstone', { title: 'Express' });
});

router.get('/grad_cap', function(req, res, next) {
  res.render('projects_grad_cap', { title: 'Express' });
});

router.get('/project_portfolio', function(req, res, next) {
  res.render('project_portfolio', { title: 'Express' });
});

router.get('/smart_mirror', function(req, res, next) {
  res.render('projects_smart_mirror', { title: 'Express' });
});

router.get('/parallel_genetic_algorithm', function(req, res, next) {
  res.render('projects_parallel_genetic_algorithm', { title: 'Express' });
});
module.exports = router;
